#!/usr/bin/env node

import { App } from "aws-cdk-lib";
import { EraserAppConfigStack } from "../lib/eraser-appconfig-stack";
import { EraserLambdaStack } from "../lib/eraser-lambda-stack";
import { EraserEventBridgeStack } from "../lib/eraser-eventbridge-stack";
import { BuildConfig } from "../lib/build-config";

const app = new App();
const application = app.node.tryGetContext("application");
const featureBranchName = app.node.tryGetContext("featureBranchName");

/*
 ** Uses a string as an object to get specific value based on propName key
 */
function ensureString(
  object: { [name: string]: any },
  propName: string,
): string {
  if (!object[propName] || object[propName].trim().length === 0)
    throw new Error(propName + " does not exist or is empty");

  return object[propName];
}

export function getConfig(): BuildConfig {
  const env = app.node.tryGetContext("config");
  if (!env) {
    throw new Error(
      "Context variable missing on CDK command. Pass in as `-c config=XXX`",
    );
  }

  const environmentsBloc = app.node.tryGetContext("environments");
  const unparsedEnv = environmentsBloc[env];

  return new BuildConfig(
    application,
    env,
    featureBranchName,
    ensureString(unparsedEnv, "AWSRegion"),
    ensureString(unparsedEnv, "AWSEnv"),
  );
}

const config: BuildConfig = getConfig();

new EraserAppConfigStack(
  app,
  `${application}-appconfig-${config.getSuffixForAWSName()}-stack`,
  { env: { region: config.AWSRegion } },
  config,
);
new EraserLambdaStack(
  app,
  `${application}-lambda-${config.getSuffixForAWSName()}-stack`,
  { env: { region: config.AWSRegion } },
  config,
);
new EraserEventBridgeStack(
  app,
  `${application}-eventbrigde-${config.getSuffixForAWSName()}-stack`,
  { env: { region: config.AWSRegion } },
  config,
);
