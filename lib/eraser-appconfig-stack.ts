import { Stack, StackProps } from "aws-cdk-lib";
import { Construct } from "constructs";
import {
  CfnApplication,
  CfnConfigurationProfile,
  CfnDeploymentStrategy,
  CfnEnvironment,
} from "aws-cdk-lib/aws-appconfig";
import { Rule, Schedule } from "aws-cdk-lib/aws-events";
import { AwsApi } from "aws-cdk-lib/aws-events-targets";
import { BuildConfig } from "./build-config";

export class EraserAppConfigStack extends Stack {
  /**
   *
   * @param {Construct} scope
   * @param {string} id
   * @param {StackProps=} props
   */
  constructor(
    scope: Construct,
    id: string,
    props: StackProps,
    config: BuildConfig,
  ) {
    super(scope, id, props);

    // The code that defines your stack goes here

    const cfnApplication = new CfnApplication(
      this,
      `${config.Application}-${config.Env}-${config.FeatureBranchName}-appconfig-application`,
      {
        name: `${config.Application}-${config.Env}-${config.FeatureBranchName}-appconfig-application`,
        description: "ERASER AppConfig Application",
      },
    );

    const cfnEnvironment = new CfnEnvironment(
      this,
      `${config.Application}-${config.Env}-${config.FeatureBranchName}-appconfig-environment`,
      {
        applicationId: cfnApplication.ref,
        name: `${config.Application}-${config.Env}-${config.FeatureBranchName}-appconfig-environment`,
        description: "ERASER AppConfig Environment",
      },
    );

    const cfnConfigurationProfile = new CfnConfigurationProfile(
      this,
      `${config.Application}-${config.Env}-${config.FeatureBranchName}-appconfig-configuration-profile`,
      {
        applicationId: cfnApplication.ref,
        name: `${config.Application}-${config.Env}-${config.FeatureBranchName}-appconfig-configuration-profile`,
        description: "ERASER AppConfig Configuration Profile",
        locationUri: "hosted",
        type: "AWS.AppConfig.FeatureFlags",
      },
    );

    const cfnDeploymentStrategy = new CfnDeploymentStrategy(
      this,
      `${config.Application}-${config.Env}-${config.FeatureBranchName}-appconfig-deployment-strategy`,
      {
        name: `${config.Application}-${config.Env}-${config.FeatureBranchName}-appconfig-deployment-strategy`,
        description: "ERASER AppConfig Deployment Strategy",
        growthFactor: 100,
        deploymentDurationInMinutes: 0,
        finalBakeTimeInMinutes: 0,
        replicateTo: "NONE",
      },
    );

    new Rule(
      this,
      `${config.Application}-${config.Env}-${config.FeatureBranchName}-eventbridge-appconfig-rule`,
      {
        ruleName: `${config.Application}-${config.Env}-${config.FeatureBranchName}-eventbridge-appconfig-rule`,
        schedule: Schedule.cron({ minute: "5" }),
        targets: [
          new AwsApi({
            action: "StartDeployment",
            service: "AppConfig",
            parameters: {
              ApplicationId: cfnApplication.ref,
              ConfigurationProfileId: cfnConfigurationProfile.ref,
              ConfigurationVersion: "MyData",
              DeploymentStrategyId: cfnDeploymentStrategy.ref,
              EnvironmentId: cfnEnvironment.ref,
            },
          }),
        ],
      },
    );
  }
}
