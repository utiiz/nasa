import { Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { BuildConfig } from './build-config';

export class EraserLambdaStack extends Stack {
  /**
   *
   * @param {Construct} scope
   * @param {string} id
   * @param {StackProps=} props
   */
  constructor(
    scope: Construct,
    id: string,
    props: StackProps,
    config: BuildConfig
  ) {
    super(scope, id, props);
    // The code that defines your stack goes here
  }
}

