export class BuildConfig {
  /**
   *
   * @param {string} Application
   * @param {string} FeatureBranchName
   */
  constructor(
    readonly Application: string,
    readonly Env: string,
    readonly FeatureBranchName: string,
    readonly AWSRegion: string,
    readonly AWSEnv: string
  ) {}

  public getSuffixForAWSName() {
    return this.AWSEnv + "-" + this.getEnvName();
  }

  public getEnvName(): string {
    return this.FeatureBranchName ?? this.Env;
  }
}

