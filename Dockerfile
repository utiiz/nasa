FROM node:18-slim

RUN apt-get update && \
    apt-get -y install git && \
    apt-get clean
