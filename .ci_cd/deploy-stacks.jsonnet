local default() = {
  cache: {
    key: '$PARENT_PIPELINE_ID',
    paths: ['node_modules/']
  }
};

local deploy_all(env) =  {
    stage: 'deploy',
    image: 'registry.gitlab.com/utiiz/eraser:latest',
    variables: {
        AWS_ENVIRONMENT_CONFIG: env
    },
    extends: ['.with-aws-auth'],
    script: [
        'echo %(env)s' % { env: env },
        'echo $PARENT_PIPELINE_ID',
        'npm run deploy -- --branch $CI_COMMIT_REF_SLUG --env $AWS_ENVIRONMENT_CONFIG --stack all'
    ],
    needs: { 
        @'pipeline': '$PARENT_PIPELINE_ID', 
        job: 'build' 
    }
};

local deploy_stack(env, stack) =  {
    stage: 'deploy',
    image: 'registry.gitlab.com/utiiz/eraser:latest',
    variables: {
        AWS_ENVIRONMENT_CONFIG: env
    },
    extends: ['.with-aws-auth'],
    script: [
        'echo %(env)s' % { env: env },
        'echo %(stack)s' % { stack: stack },
        'npm run deploy -- --branch $CI_COMMIT_REF_SLUG --env $AWS_ENVIRONMENT_CONFIG --stack %(stack)s' % { stack: stack }
    ],
    needs: { 
        @'pipeline': '$PARENT_PIPELINE_ID', 
        job: 'build' 
    },
    rules: [
        {
            when: 'manual',
        },
    ],
};

local stacks = {
    envs: [
        { name: 'dev', stacks: ['appConfig', 'eventBridge'] },
        { name: 'pprod', stacks: ['appConfig', 'eventBridge'] },
        { name: 'prod', stacks: ['appConfig', 'eventBridge'] },
    ]
};

{
    ['%(env)s-stacks.yml' % { env: env.name }]:
    {
        ['default']: default(),
        ['deploy-aws-%(env)s' % { env: env.name }]: deploy_all(env.name)
    }
    {
        ['deploy-aws-%(env)s-%(stack)s' % { env: env.name, stack: stack }]: deploy_stack(env.name, stack)
        for stack in env.stacks
    }
    for env in stacks.envs
}
