import "zx/globals";

const envs = ["dev", "pprod", "prod"];
const current_branch = argv.branch;
const profile = argv.profile;
const env = argv.env || "dev";

if (!envs.includes(env)) {
  echo("You are not on an allowed environment, aborting deploy");
  await $`exit 1`;
}

if (!envs.includes(current_branch) && !current_branch.startsWith("eraser-")) {
  echo("You are not on an allowed branch, aborting deploy");
  await $`exit 1`;
}
let PROFILE_OPT = profile ? "--profile=".concat(profile) : "";

echo(`Déploiement de la stack sur la branche ${current_branch}`);
await $`npx cdk deploy -c application=eraser -c config=${env} -c featureBranchName=${current_branch} --role-arn="arn:aws:iam::267034929058:role/cf-app-eraser-deploy-role" ${PROFILE_OPT} --require-approval never --all`;
